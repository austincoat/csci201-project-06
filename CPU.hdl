// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/05/CPU.hdl

/**
 * The Hack CPU (Central Processing unit), consisting of an ALU,
 * two registers named A and D, and a program counter named PC.
 * The CPU is designed to fetch and execute instructions written in 
 * the Hack machine language. In particular, functions as follows:
 * Executes the inputted instruction according to the Hack machine 
 * language specification. The D and A in the language specification
 * refer to CPU-resident registers, while M refers to the external
 * memory location addressed by A, i.e. to Memory[A]. The inM input 
 * holds the value of this location. If the current instruction needs 
 * to write a value to M, the value is placed in outM, the address 
 * of the target location is placed in the addressM output, and the 
 * writeM control bit is asserted. (When writeM==0, any value may 
 * appear in outM). The outM and writeM outputs are combinational: 
 * they are affected instantaneously by the execution of the current 
 * instruction. The addressM and pc outputs are clocked: although they 
 * are affected by the execution of the current instruction, they commit 
 * to their new values only in the next time step. If reset==1 then the 
 * CPU jumps to address 0 (i.e. pc is set to 0 in next time step) rather 
 * than to the address resulting from executing the current instruction.  
 */

/* Required: 16 gates or fewer */
/* Extra credit: 15 gates */

CHIP CPU {

    IN  inM[16],         // M value input  (M = contents of RAM[A])
        instruction[16], // Instruction for execution
        reset;           // Signals whether to re-start the current
                         // program (reset==1) or continue executing
                         // the current program (reset==0).

    OUT outM[16],        // M value output
        writeM,          // Write to M? 
        addressM[15],    // Address in data memory (of M)
        pc[15];          // address of next ins truction

    PARTS:
    
	Mux16(a=instruction,b=aluout,sel=instruction[15],out=mout1);
	Mux(a=true,b=instruction[5],sel=instruction[15],out=c);
	ARegister(in=mout1,load=c,out=outrega,out[0..14]=addressM);
	Mux16(a=outrega,b=inM,sel=instruction[12],out=outmrega);
	And(a=instruction[15],b=instruction[4],out=d);
	DRegister(in=aluout,load=d,out=outregd); 
	ALU(x=outregd, y=outmrega, zx=instruction[11], nx=instruction[10],zy=instruction[9], ny=instruction[8], f=instruction[7],no=instruction[6], out=aluout, zr=ZRout, ng=NGout, out=outM);
    Nor(a=ZRout,b=NGout,out=JGT);
	Not(in=NGout,out=JGE);
	Not(in=ZRout,out=JNE);
	Or(a=ZRout,b=NGout,out=JLE);
	Mux8Way(a=false,b=JGT,c=ZRout,d=JGE,e=NGout,f=JNE,g=JLE,h=true,sel=instruction[0..2],out=jump);
	And(a=instruction[15],b=jump,out=loadit);
	PC(in=outrega,inc=true, load=loadit, reset=reset, out[0..14]=pc);
	And(a=instruction[15], b=instruction[3], out=writeM);
}
